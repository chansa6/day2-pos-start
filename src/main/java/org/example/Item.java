package org.example;

public class Item {
    private final String name;
    private final float unitPrice;
    private final String unit;

    // Constructor
    public Item(String name, float unitPrice, String unit) {
        this.name = name;
        this.unitPrice = unitPrice;
        this.unit = unit;
    }

    // Decode InfoString
    public static Item decodeInfoString(String infoString) {
        String[] parts = infoString.split(",");
        String name = parts[0].trim();
        float unitPrice = Float.parseFloat(parts[1].trim());
        String unit = parts[2].trim();
        return new Item(name, unitPrice, unit);
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public float getUnitPrice() {
        return unitPrice;
    }
}