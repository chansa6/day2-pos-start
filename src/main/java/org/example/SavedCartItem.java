package org.example;

public class SavedCartItem {
    private final String barcode;
    private final Item item;
    private final int saved_quantity;
    private final float value;

    public SavedCartItem(CartItem card_item) {
        this.barcode = card_item.getBarcode();
        this.item = card_item.getItem();
        this.saved_quantity = computeSavedQuantity(card_item.getQuantity());
        this.value = this.saved_quantity * this.item.getUnitPrice();
    }

    public int computeSavedQuantity(float quantity) {
        return (int) (quantity / 3);
    }

    public Item getItem() {
        return item;
    }

    public int getSaved_quantity() {
        return saved_quantity;
    }

    public float getValue() {
        return value;
    }
}
