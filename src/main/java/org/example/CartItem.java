package org.example;

public class CartItem {
    private final String barcode;
    private final Item item;
    private final float quantity;
    private final float subtotal;

    public CartItem(String barcode, float quantity, Item item) {
        this.barcode = barcode;
        this.quantity = quantity;
        this.item = item;
        this.subtotal = computeSubtotal();

    }

    public Item getItem() {
        return item;
    }

    public String getBarcode() {
        return barcode;
    }

    public float getQuantity() {
        return quantity;
    }

    public float getSubtotal() {
        return subtotal;
    }

    public float computeSubtotal() {
        assert item != null;
        return quantity * item.getUnitPrice();
    }
}
