package org.example;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Printer {
    public String print(String[] cart, String[] promotions) {
        Map<String, Float> cart_item_quantities = generateQuantities(List.of(cart));
        List<String> barcodes_in_cart_sorted = getSortedBarcodesInCart(cart_item_quantities);
        Map<String, Item> cart_items = getItemsInCart(barcodes_in_cart_sorted);
        Map<String, CartItem> cart_metadata = generateCartMetadata(cart_items, cart_item_quantities);
        Map<String, SavedCartItem> promoted_metadata = generatePromotedMetadata(List.of(promotions), cart_metadata);

        return formatReceipt(cart_metadata, promoted_metadata);
    }


    public List<String> getSortedBarcodesInCart(Map<String, Float> item_quantities) {
        return item_quantities.keySet().stream().sorted().collect(Collectors.toList());
    }


    public Map<String, Item> getItemsInCart(List<String> sorted_barcodes_in_cart) {
        Map<String, String> all_items = PosDataLoader.loadAllItems();
        return sorted_barcodes_in_cart.stream().filter(all_items::containsKey).collect(Collectors.toMap(
                barcode -> barcode, barcode -> Item.decodeInfoString(all_items.get(barcode)),
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new
                ));
    }

    public Map<String, CartItem> generateCartMetadata(Map<String, Item> items_in_cart, Map<String, Float> item_quantities) {
        Map<String, CartItem> cart_meta_data = new LinkedHashMap<>();

        for (String barcode : items_in_cart.keySet()) {
            Float quantity = item_quantities.get(barcode);
            if (quantity != null) {
                Item item = items_in_cart.get(barcode);
                CartItem cart_item = new CartItem(barcode, quantity, item);
                cart_meta_data.put(barcode, cart_item);
            }
        }

        return cart_meta_data;
    }


    public float getQuantityFromBarcode(String barcode) {
        return barcode.contains("-") ? Float.parseFloat(barcode.substring(barcode.indexOf("-")+1)) : 1;
    }


    public Map<String, Float> generateQuantities(List<String> barcodes) {
        return barcodes.stream()
                .collect(Collectors.toMap(
                        barcode -> barcode.contains("-") ? barcode.substring(0, barcode.indexOf('-')) : barcode,
                        this::getQuantityFromBarcode,
                        Float::sum));
    }


    public Map<String, SavedCartItem> generatePromotedMetadata(List<String> promoted_barcodes, Map<String, CartItem> cart_metadata) {
        Map<String, SavedCartItem> promoted_metadata = new LinkedHashMap<>();

        for (Map.Entry<String, CartItem> entry : cart_metadata.entrySet()) {
            float quantity = entry.getValue().getQuantity();
            if (promoted_barcodes.contains(entry.getKey()) && Math.floor(quantity) > 3 &&
                    quantity == Math.floor(quantity)) {
                promoted_metadata.put(entry.getKey(), new SavedCartItem(entry.getValue()));
            }
        }

        return promoted_metadata;
    }


    public float computeGrandTotal(Map<String, CartItem> card_metadata) {
        return (float) card_metadata.values().stream()
                .mapToDouble(CartItem::getSubtotal)
                .sum();
    }

    public float computeSavedTotal(Map<String, SavedCartItem> saved_metadata) {
        return (float) saved_metadata.values().stream()
                .mapToDouble(SavedCartItem::getValue)
                .sum();
    }



    public String generateItemReceiptLine(CartItem cart_item) {
        float quantity = cart_item.getQuantity();
        String quantityFormat = (quantity == Math.floor(quantity)) ? "%.0f" : "%.1f"; // distinguish between integer and floating quantity
        return String.format("Name：%s，Quantity：" + quantityFormat + " %s，" +
                "Unit Price：%.2f(CNY)，Subtotal：%.2f(CNY)\n", cart_item.getItem().getName(), cart_item.getQuantity(),
                cart_item.getItem().getUnit() + (cart_item.getQuantity() > 1 ? "s" : ""), cart_item.getItem().getUnitPrice(), cart_item.getSubtotal());
    }


    public String generatePromotedItemReceiptLine(SavedCartItem promoted_cart_item) {
        int saved_quantity = promoted_cart_item.getSaved_quantity();
        return String.format("Name：%s，Quantity：%d %s，" +
                        "Value：%.2f(CNY)\n", promoted_cart_item.getItem().getName(), saved_quantity,
                promoted_cart_item.getItem().getUnit() + (saved_quantity > 1 ? "s" : ""), promoted_cart_item.getItem().getUnitPrice() * saved_quantity);
    }


    public String generateTotalLines(float grand_total, float saved_total) {
        String total_lines = String.format("Total：%.2f(CNY)\n", grand_total-saved_total);
        if (saved_total > 0) {
            total_lines += String.format("Saved：%.2f(CNY)\n", saved_total);
        }
        return total_lines;
    }


    public String formatReceipt(Map<String, CartItem> cart_metadata, Map<String, SavedCartItem> promoted_metadata) {
        String receipt = "***<No Profit Store> Shopping List***\n" + "----------------------\n";
        receipt += cart_metadata.values().stream().map(this::generateItemReceiptLine).collect(Collectors.joining(""));
        receipt += "----------------------\n" + "Buy two get one free items：\n";
        receipt += promoted_metadata.values().stream().map(this::generatePromotedItemReceiptLine).collect(Collectors.joining(""));
        receipt += "----------------------\n";
        receipt += generateTotalLines(computeGrandTotal(cart_metadata), computeSavedTotal(promoted_metadata));
        receipt += "**********************\n";
        return receipt;
    }


}

