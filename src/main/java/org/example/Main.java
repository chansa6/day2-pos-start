package org.example;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Printer printer = new Printer();
//        List<String> barcodes = Arrays.asList("abc", "abc-1.5", "def-2.5", "abc");
        String[] barcodes = {"ITEM000001", "ITEM000001", "ITEM000001", "ITEM000001",
                "ITEM000001", "ITEM000003", "ITEM000003", "ITEM000005-3", "ITEM000006-3.4"};
        System.out.println(printer.generateQuantities(List.of(barcodes)));


        String info_string = "Instant noodles,4.5,pack";
        Item item = Item.decodeInfoString(info_string);

        // Access the properties of the decoded item
         System.out.println(item.getName());
         System.out.println(item.getUnitPrice());
         System.out.println(item.getUnit());

        String[] sorted_barcodes_in_cart = {"ITEM000001", "ITEM000003", "ITEM000005", "ITEM000006"};
        Map<String, Item> items_in_cart = printer.getItemsInCart(List.of(sorted_barcodes_in_cart));
        Map<String, Float> item_quantities = printer.generateQuantities(List.of(barcodes));
        Map<String, CartItem> cart_meta_data = printer.generateCartMetadata(items_in_cart, item_quantities);

        for (Map.Entry<String, CartItem> entry : cart_meta_data.entrySet()) {
            System.out.println("Barcode: " + entry.getKey() + ", CartItem: " + entry.getValue());
        }

        for (CartItem cartItem : cart_meta_data.values()) {
            System.out.println("Name: " + cartItem.getItem().getName()
                    + "; UnitPrice: " + cartItem.getItem().getUnitPrice()
                    + ": Quantity: " + cartItem.getQuantity()
                    + "; Subtotal: " + cartItem.getSubtotal());
        }

        System.out.println("Seeing whole flow");

        String[] cart = PosDataLoader.loadCart();
        String[] promotions = PosDataLoader.loadPromotion();

        System.out.println(printer.print(cart, promotions));


    }
}